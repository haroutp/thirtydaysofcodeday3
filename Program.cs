﻿using System;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            int N = Convert.ToInt32(Console.ReadLine());

            if(N % 2 != 0){
                System.Console.WriteLine("Weird");
            }else if((N >= 2 & N <= 5) | N > 20){
                System.Console.WriteLine("Not Weird");
            }else if(N >= 6 & N <= 20){
                System.Console.WriteLine("Weird");
            }
        }
    }
}
